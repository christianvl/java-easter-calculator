package calculateeaster;

import java.util.Calendar;

/**
 * Class to calculate the Easter date of any given year based on the algorithm
 * of the United States Naval Observatory.
 *
 * @author Christian van Langendonck
 */
public class CalculateEaster {

    /**
     * Class Calendar that will hold the Easter date information (day of month,
     * month, year).
     */
    private Calendar easter;

    /**
     * Constructor parameterized with a String representing a year (should be
     * between 1500 and 9999). String must be numeric only.
     *
     * @param year String YYYY
     */
    public CalculateEaster(String year) {
        setEaster(year);
    }

    /**
     * Constructor parameterized with a calendar
     *
     * @param cal Calendar (only information required is year, should be between
     *            1500 and 9999)
     */
    public CalculateEaster(Calendar cal) {
        this(String.valueOf(cal.get(Calendar.YEAR)));
    }

    /**
     * Takes a String representing a year (YYYY - between 1500 and 9999) and sets
     * the calendar with the Easter date for that year. String must be convertible
     * to a number.
     *
     * @param year String (YYYY - between 1500 and 9999)
     */
    public void setEaster(String year) {
        int c, d, n, k, i, j, l, m, y;
        if (isValidYear(year)) {
            y = Integer.valueOf(year);
            // Algorithm by the US Naval Observatory
            c = y / 100;
            n = y - 19 * (y / 19);
            k = (c - 17) / 25;
            i = c - c / 4 - (c - k) / 3 + 19 * n + 15;
            i = i - 30 * (i / 30);
            i = i - (i / 28) * (1 - (i / 28) * (29 / (i + 1)) * ((21 - n) / 11));
            j = y + y / 4 + i + 2 - c + c / 4;
            j = j - 7 * (j / 7);
            l = i - j;
            m = 3 + (l + 40) / 44;
            d = l + 28 - 31 * (m / 4);
            if (easter == null) {
                this.easter = Calendar.getInstance();
            }
            this.easter.set(y, m, d);
        } else {
            System.out.println("Invalid year: " + year);
        }
    }

    /**
     * Getter for the Easter Calendar object created in this class.
     *
     * @return Calendar
     */
    public Calendar getEaster() {
        return easter;
    }

    /**
     * This method checks if the year passed as an argument is a valid String and
     * year.
     *
     * @param year String as YYYY and between 1500 and 9999
     * @return true if the year is valid
     */
    private boolean isValidYear(String year) {
        try {
            if (year == null || year.isEmpty() || Integer.valueOf(year) < 1900 || Integer.valueOf(year) > 9999) {
                return false;
            }
        } catch (NumberFormatException error) {
            return false;
        }
        Calendar check = Calendar.getInstance();
        check.setLenient(true);
        try {
            check.set(Calendar.YEAR, Integer.valueOf(year));
        } catch (Exception error) {
            return false;
        }
        return true;
    }

    /**
     * To String method returns the representation date of the Calendar in the form
     * YYYY/MM/DD.
     *
     * @return String YYYY/MM/DD
     */
    @Override
    public String toString() {
        return easter.get(Calendar.YEAR) + "/" + easter.get(Calendar.MONTH) + "/" + easter.get(Calendar.DAY_OF_MONTH);
    }

}

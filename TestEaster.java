
import calculateeaster.CalculateEaster;
import java.util.Calendar;
//import java.util.Scanner;

public class TestEaster {

    public static void main(String args[]) {
        Calendar tester = Calendar.getInstance();
        tester.set(Calendar.YEAR, 2021);
        CalculateEaster easterDate = new CalculateEaster(tester);
        System.out.println(
                "Easter date for year " + easterDate.getEaster().get(Calendar.YEAR) + " is " + easterDate.toString());

        String year = "2022";
        CalculateEaster easterDate2 = new CalculateEaster(year);
        System.out.println("Easter date for year " + year + " is " + easterDate2.toString());
    }
}
